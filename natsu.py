import random
import operator

def read_from_txt_file(f):
	sents_wlabel = []
	for sent in f:
		no, verbatim, expected_str, predicted_str = sent.split(';')
		expected_list  = expectedHandler(expected_str)
		predicted_list = predictedHandler(predicted_str)
		#print([no, verbatim, expected_list, predicted_list])
		sents_wlabel.append([no, verbatim, expected_list, predicted_list])
	return sents_wlabel


def expectedHandler(data):
	elements = data.replace('[', '').replace(']', '').replace('\'','').split(', ')
	elements = [elements[x:x+4] for x in range(0, len(elements), 4)]
	elements = [[subc, sentiment] for [no, mainc, subc, sentiment] in elements]
	return elements


def predictedHandler(data):
	elements = data.replace('[', '').replace(']', '').replace('\'','').split(', ')
	elements = [elements[x:x+4] for x in range(0, len(elements), 4)]
	elements = [[subc, sentiment] for [subc, subcconf, sentiment, stmconf] in elements]
	# # duplicate output
	# elements_uniq = []
	# elements_check = []
	# for subc, sentiment, subcconf, stmconf in elements:
	# 	if [subc, sentiment] not in elements_check:
	# 		elements_uniq.append([subc, sentiment, subcconf, stmconf])
	# 		elements_check.append([subc, sentiment])
	return elements

# delete duplicate element in list
def uniq(input):
	output = []
	for x in input:
		if x not in output:
			output.append(x)
	return output
'''
	..Main function..
'''


# 0. read input file to list (temporary section)
filename = "20170503to_mark.txt"

f = open(filename, encoding="utf8")

sents_wlabel = read_from_txt_file(f)



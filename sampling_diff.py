import random
import operator
from natsu import read_from_txt_file

def is_diff(expected, predicted):
	#print("expected:", expected, " predicited", predicted)
	diff_result = 0
	if len(expected) != len(predicted):
		diff_result = 1
	else:
		sorted_expected = sorted(expected, key = operator.itemgetter(1, 0))
		sorted_predicted = sorted(predicted, key = operator.itemgetter(1, 0))
		#print("sorted_expected:", sorted_expected, " sorted_predicted", sorted_predicted)
		if sorted_expected == sorted_predicted:
			diff_result = 0
		else:
			diff_result = 1

	return diff_result

def sampling_diff_from_list(sents_wlabel, number_of_wanted_diff):
	result_list = create_zero_list(len(sents_wlabel))
	sampling_diff_item_list = []
	sampling_diff_sents_wlabel_indexs = []
	diff_item_list = []
	diff_item_sents_wlabel_indexs = []

	#get diff items
	for i in range(0, len(sents_wlabel)):
		expected = sents_wlabel[i][2]
		predicted = sents_wlabel[i][3]

		if is_diff(expected, predicted):
			diff_item_sents_wlabel_indexs.append(i);
			diff_item_list.append(sents_wlabel[i])

	# TODO remove is_label = True from diff_item_list

	#sampling {number_of_wanted_diff} indexes from number_of_wanted_diff
	if number_of_wanted_diff > len(diff_item_list):
		diff_item_sampling_item_count = len(diff_item_list)
	else:
		diff_item_sampling_item_count = number_of_wanted_diff

	diff_item_list_sampling_indexes = random.sample(range(0, len(diff_item_list)), diff_item_sampling_item_count)

	#create samling diff item list
	for index in diff_item_list_sampling_indexes:
		sampling_diff_sents_wlabel_indexs.append(diff_item_sents_wlabel_indexs[index])
		sampling_diff_item_list.append(diff_item_list[index])

	# create result_list
	for i in range(0, len(sampling_diff_sents_wlabel_indexs)):
		result_list[sampling_diff_sents_wlabel_indexs[i]] = 1

	#print(result_list)
	return result_list

def create_zero_list(n):
    listofzeros = [0] * n
    return listofzeros


'''
	..Main function..
'''


# 0. read input file to list (temporary section)
filename = "20170503to_mark.txt"

f = open(filename, encoding="utf8")

sents_wlabel = read_from_txt_file(f)
print(sampling_diff_from_list(sents_wlabel, 2))


from natsu import read_from_txt_file

def mapSubAttributeToIndex(numClass):
    return {
        '1.1': 0, '1.2': 1, '1.3': 2, '1.4': 3, '1.5': 4,
        '2.1': 5, '2.2': 6, '2.3': 7,
        '3.1': 8, '3.2': 9, '3.3': 10,
        '4.1': 11, '4.2': 12, '4.3': 13, '4.4': 14, '4.5': 15,
        '4.6': 16, '4.7': 17, '4.8': 18, '4.9': 19,
        '5.1': 20, '5.2': 21, '5.3': 22, '5.4': 23, '5.5': 24, '5.6': 25,
        '6.1': 26,
        '7.1': 27, '7.2': 28, '7.3': 29,
        '8.1': 30
    }[numClass]


def mapIndexToSubAttribute(numClass):
    return {
        0: '1.1', 1: '1.2', 2: '1.3', 3: '1.4', 4: '1.5',
        5: '2.1', 6: '2.2', 7: '2.3',
        8: '3.1', 9: '3.2', 10: '3.3',
        11: '4.1', 12: '4.2', 13: '4.3', 14: '4.4', 15: '4.5',
        16: '4.6', 17: '4.7', 18: '4.8', 19: '4.9',
        20: '5.1', 21: '5.2', 22: '5.3', 23: '5.4', 24: '5.5', 25: '5.6',
        26: '6.1',
        27: '7.1', 28: '7.2', 29: '7.3',
        30: '8.1'
    }[numClass]


def create_confusion_matrix_dict():
    confuse_dict_result = {}

    for i in range(0, 31):
        confuse_inner_dict = {}
        for j in range(0, 31):
            confuse_inner_dict[mapIndexToSubAttribute(j)] = []
            confuse_dict_result[mapIndexToSubAttribute(i)] = confuse_inner_dict
    return confuse_dict_result


# print(confuse_dict)


def compute_confusion_matrix(sents_wlabel):
    # input sents_wlabel from file
    confuse_dict_compute = create_confusion_matrix_dict()

    for sent_item in sents_wlabel:
        #print("\n", sent_item)
        sent_id, verbatim, expected_list, predicted_list = sent_item

        # print(sent_id)
        # print(expected_list)
        # print(predicted_list)

        expected_subclass_list = []
        predicted_subclass_list = []
        for expected_item in expected_list:
            expected_subclass_list.append(expected_item[0])

        for predicted_item in predicted_list:
            predicted_subclass_list.append(predicted_item[0])

        #print("sent_id:", sent_id)
        #print("expected_subclass_list:", expected_subclass_list)
        #print("predicted_subclass_list:", predicted_subclass_list)
        for expected_subclass in expected_subclass_list[:]:
            if expected_subclass in predicted_subclass_list:
                expected_subclass_list.remove(expected_subclass)
                predicted_subclass_list.remove(expected_subclass)
                confuse_dict_compute[expected_subclass][expected_subclass].append(sent_id)
                #print("True:", expected_subclass, expected_subclass, confuse_dict_compute[expected_subclass][expected_subclass])

        for expected_subclass in expected_subclass_list[:]:
            # print("predicted_subclass_list", predicted_subclass_list)
            for predicted_subclass in predicted_subclass_list:
                confuse_dict_compute[expected_subclass][predicted_subclass].append(sent_id)
                #print("False:", expected_subclass, predicted_subclass, confuse_dict_compute[expected_subclass][predicted_subclass])

    return confuse_dict_compute

def print_existing_ids_in_confusion_dict(computed_confuse_dict):
    for i in range(0, 31):
        expected_sub_class = mapIndexToSubAttribute(i)
        for j in range(0, 31):
            predicted_sub_class = mapIndexToSubAttribute(j)
            if len(computed_confuse_dict[expected_sub_class][predicted_sub_class]) > 0:
                print("confuse_dict[{0}][{1}]:".format(expected_sub_class, predicted_sub_class),
                      computed_confuse_dict[expected_sub_class][predicted_sub_class])



'''
	..Main function..
'''


# 0. read input file to list (temporary section)
filename = "20170503to_mark.txt"

f = open(filename, encoding="utf8")

sents_wlabel = read_from_txt_file(f)

confuse_dict = compute_confusion_matrix(sents_wlabel)
print_existing_ids_in_confusion_dict(confuse_dict)
